#Cadenas simples
cadena1 = '.-Una "cadena" de texto'
cadena2 = ".-Una \"cadena \'de' texto"

#Cadenas multiples lineas
cadena3 = '''.-Una cadena
de varias 
lineas'''

cadena4 = """.-Una cadena
de varias 
lineas"""

""" un
comentario
multiple"""

print(cadena1)
print(cadena2)
print(cadena3)
print(cadena4)
