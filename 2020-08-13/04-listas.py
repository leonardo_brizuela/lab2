lista1 = [0, 5, 2, 5, -1, 10, 3, 2]

print(lista1)

lista1.append(45)
print(lista1)

lista1.extend([-3, -7])
print(lista1)

lista1.insert(2, 6)
print(lista1)

lista1.reverse()
print(lista1)

lista1.sort()
print(lista1)

# Quitar por indice
lista1.pop(8)
print(lista1)

# Quita por elemento
lista1.remove(5)
print(lista1)

print(len(lista1))

print(0 in lista1)
