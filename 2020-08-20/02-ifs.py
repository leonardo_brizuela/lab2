# Pedir un mes al usuario (Enero-diciembre)
# Mostrar a que estacion pertenece (verano, otoño, invierno, primavera)
# Verano: Enero, Febrero, Marzo
# Otoño: Abril, Mayo, Junio
# Invierno: Julio, Agosto, Septiempre
# Primavera: Octubre, Noviembre, Diciembre

mes = input('Ingrese mes: ').lower()

if mes in ('enero', 'febrero', 'marzo'):
    print('Verano')
elif mes in ('abril', 'mayo', 'junio'):
    print('otoño')
