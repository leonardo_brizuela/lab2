# break funciona para for y while

# Cargar en una lista palabras hasta que se ingrese una palabra vacia
# Mostrar palabras ordenadas

lista = []
# "do-while" en python
while True:
    palabra = input('Ingrese palabra: ')
    if palabra == '':
        break
    lista.append(palabra)

lista.sort()

for item in lista:
    print(item)
