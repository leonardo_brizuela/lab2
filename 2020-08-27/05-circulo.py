# Pedir radio al usuario
# Calcular diametro, perimetro y area
from circulo import diametro, perimetro, area, circulo

radio = float(input("Ingrese radio: "))

# print("Diametro: ", diametro(radio))
# print("Perimetro: ", perimetro(radio))
# print("Area: ", area(radio))

diam, per, ar = circulo(radio)

print("Diametro: ", diam)
print("Perimetro: ", per)
print("Area: ", ar)
