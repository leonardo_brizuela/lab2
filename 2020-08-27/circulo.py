from math import pi

# modulo circulo
# funcion diametro (radio)
# funcion perimetro (radio)
# funcion area (radio)


def diametro(radio):
    return radio * 2


def perimetro(radio):
    return pi * radio * 2


def area(radio):
    return pi * (radio ** 2)


def circulo(radio):
    return diametro(radio), perimetro(radio), area(radio)
