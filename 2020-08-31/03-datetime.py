# Uso basico de datetime
from datetime import datetime

d = datetime.now()

print("Fecha y hora actual: ", d)

print("Año: ", d.year)
print("Mes: ", d.month)
print("Día: ", d.day)
