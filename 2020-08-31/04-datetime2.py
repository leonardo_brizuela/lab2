# Dar formato a fecha y hora
from datetime import datetime

ahora = datetime.now()
print("Ahora: ", ahora)

print("Fecha: ", ahora.strftime("%d/%m/%Y"))
print("Fecha: ", ahora.strftime("%x"))

print("Hora: ", ahora.strftime("%I:%M:%S %p"))
print("Hora: ", ahora.strftime("%X"))
