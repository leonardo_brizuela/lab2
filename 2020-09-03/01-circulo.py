# Pedir radio al usuario
# Calcular diametro, perimetro y area
from circulo import Circulo

radio = float(input("Ingrese radio: "))

circulo = Circulo(radio)

print("Diametro: ", circulo.diametro())
print("Perimetro: ", circulo.perimetro())
print("Area: ", circulo.area())
