# Clase Personas
# Parametro: lista_personas
# Metodos:
#  agregar: Agrega un objeto persona a la lista
#  mayor: Devuelve la persona mayor
#  menor: Devuelve la persona menor

def func_edad(persona):
    return persona.edad


class Personas:
    lista_personas = []

    def agregar(self, persona):
        self.lista_personas.append(persona)

    def mayor(self):
        edad_mayor = self.lista_personas[0].edad
        indice_mayor = 0
        for i in range(1, len(self.lista_personas)):
            if self.lista_personas[i].edad > edad_mayor:
                edad_mayor = self.lista_personas[i].edad
                indice_mayor = i
        return self.lista_personas[indice_mayor]

    def menor(self):
        # Opcion 1: con sort (si se ordena lista)
        # self.lista_personas.sort(key=func_edad)
        # return self.lista_personas[0]

        # Opcion 2: con sorted (no se ordena lista)
        # lista = sorted(self.lista_personas, key=func_edad)
        # return lista[0]

        # Opcion 3: con referencia (si se ordena lista)
        lista = self.lista_personas
        lista.sort(key=func_edad)
        return lista[0]

    def mostrar_todo(self):
        for persona in self.lista_personas:
            print(persona)
