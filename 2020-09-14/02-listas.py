edades=[13,12,5,16,23,24,56,45,10,78,45,49]

def par_func(edad):
    return edad % 2 == 0


pares=list(filter(par_func,edades))
impares= (lambda edad: edad % 2 == 1)

print('Edades pares',pares)
print('Edades impares',impares)
