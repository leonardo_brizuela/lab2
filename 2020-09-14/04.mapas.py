from math import pi
angulos=[90,180,270,45,380]

def  rad_func(ang):
    return((ang * pi)/180)

angulos_radianes = list (map(rad_func,angulos))    

print ('Angulos en grados - Angulos en Radianes')
for i in range (0,len(angulos)):
    print(angulos[i],"\t - \t",angulos_radianes[i])