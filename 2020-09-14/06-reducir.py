from functools import reduce

numeros=[-4,5,2,6,2,7,0,12,5,9]

suma =0
for numero in numeros:
    suma += numero

print('La suma de numeros',suma)

def suma_func(a,b):
    print('a:',a)
    print('b:',b)
    return a+b

suma = reduce(suma_func,numeros)

print('Suma con reduce',suma)