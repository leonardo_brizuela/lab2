from functools import reduce
numeros= [-4,5,2,6,2,7,12,5,9]
Producto=0
for numero in numeros:
    Producto = (numero*numero)

print('El producto de numeros',Producto)


def prod_func(a,b):
    print('a:',a)
    print('b:',b)
    return a*b

Producto = reduce(prod_func,numeros)

print('Producto con reduce',Producto)